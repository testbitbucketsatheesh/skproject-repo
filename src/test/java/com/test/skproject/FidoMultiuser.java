package com.test.skproject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;

public class FidoMultiuser {
	
	WebDriver driver;
  // This is for 2dimensional array concept for multi-user login
  @DataProvider(name="loginmultiuser")
  public Object[][] data (){
  Object[][] users= {{"abc","123"}, {"aaa","999"}, {"bbb","333"}};
  return users;
  }
 
  @BeforeMethod
  public void beforeClass() {
  System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");
  driver= new ChromeDriver();
  driver.get("http://www.fido.ca");
  }
  
  @Test(dataProvider="loginmultiuser")
  public void users(String id, String pwd) {
  driver.findElement(By.xpath("//a[@href='javascript:void(0);']")).click();
  //driver.findElement(By.xpath("//input[@name='email']")).click();
  driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys(id);
  //driver.findElement(By.xpath("//input[@name='password']")).click();
  driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pwd);
  driver.findElement(By.xpath("//button[@type='button'][8]")).click();
  
  }

  @AfterMethod
  public void afterClass() {
  driver.quit();
  }

}
