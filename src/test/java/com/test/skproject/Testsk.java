package com.test.skproject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
public class Testsk {
	static String url;
	
  @BeforeClass	
  public void f() throws IOException {
	  
	  File file = new File("./testingurl.properties");
	  InputStream is = new FileInputStream(file);
	  
	  Properties prop = new Properties();
	  prop.load(is);
	   url = prop.getProperty("fburl");
  }
   
  @Test
  public void beforeClass() {
	  System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");
	  WebDriver driver = new ChromeDriver();
	  driver.get(url);
  }
  

  @AfterClass
  public void afterClass() {
	  url ="http://www.google.com";
	  System.out.println("The url is "+url);
  }

}
